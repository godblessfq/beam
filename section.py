import Part


def Moment_2(sketch):
    # 1 get inner and outer wire
    wires = sketch.Shape.Wires

    # check boundingbox diagonals to get outer shape
    diagonals = [wire.BoundBox.DiagonalLength for wire in wires]
    pos = diagonals.index(max(diagonals))

    # reaordering
    outer_wire = wires[pos]
    wires.pop(pos)
    wires.insert(0, outer_wire)

    Ix = 0
    Iy = 0
    for j, wire in enumerate(wires):
        pts = wire.discretize(1000)
        ix = 0
        iy = 0
        for i, pt in enumerate(pts[:-1]):
            # one point integration
            midpoint = (pts[i] + pts[i + 1]) * 0.5
            diff = pts[i] - pts[i + 1]
            ix += midpoint.y ** 3 * diff.x / 3.
            iy += midpoint.x ** 3 * diff.y / 3.


        Ix += ((j == 0)*2 - 1) * abs(ix)
        Iy += ((j == 0)*2 - 1) * abs(iy)
    
    return(Ix, Iy, Ix + Iy)


a = App.ActiveDocument.Sketch
print(Moment_2(a))
