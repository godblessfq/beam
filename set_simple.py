import FreeCAD as App

def set_simple():
    type_objects = [i for i in App.ActiveDocument.Objects if hasattr(i, "type")]
    beam_objects = [i for i in type_objects if i.type == "c_beam"]
    if beam_objects:
        cut_bool = not beam_objects[0].cut
        for beam in beam_objects:
            beam.cut = cut_bool
        App.ActiveDocument.recompute()
