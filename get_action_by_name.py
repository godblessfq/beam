import FreeCAD as App
import FreeCADGui as Gui
from PySide import QtGui


def getGuiElement(element, name):
	if element.objectName() == name:
		return element
	else:
		if hasattr(element, "children"):
			children = element.children()
			if children:
				for child in element.children():
					el = getGuiElement(child, name)
					if el: return el
	return None


def getActions(parent):
	if hasattr(parent, "children"):
		children = parent.children()
		if children:
			for child in parent.children():
				for element in getActions(child):
					yield element
		elif isinstance(parent, QtGui.QAction) and parent.objectName() and parent.objectName != "Seperator":
			yield parent

def getActionDict(root):
	actions = {}
	for action in getActions(root):
		actions[action.objectName()] = action
	return actions

def getWorkbenches(mw):
	actions = getActionDict(mw)
	for item in actions:
		if "Sketch" in item:
			yield item



mw = Gui.getMainWindow()
#print(getActionDict(mw))
a = getGuiElement(mw, "Part_Fuse")
print(a.parent())