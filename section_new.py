from __future__ import division
import numpy as np
from ppm import PanelVector2, Vector2, Panel2
from ppm.vtk_export import VtkWriter
from ppm.utils import check_path
from torsion import DirichletPanel, TorsionCase, NeumannPanel


def check_rotation(points, outer=True):
    """assuming that the sketch lies in x-y"""
    z = 0
    for i, pt in enumerate(points[:-1]):
        pt1 = points[i + 1]
        z_loc = ((pt1 - pt).cross(pt1)).z
        z += z_loc
    if z > 0 and outer:
        return points
    elif z <= 0 and (not outer):
        return points
    else:
        return points[::-1]


def check_connection(points):
    flatten = []
    if len(points) == 1:
        return points[0][:-1]
    for i, pts in enumerate(points):
        if i == 0:
            if (
                    is_same_point(pts[-1], points[i + 1][0]) or
                    is_same_point(pts[-1], points[i + 1][-1])):
                flatten += pts
            else:
                flatten += pts[::-1]
        else:
            # check if first point is the same as the last
            # point of the previous list
            if is_same_point(pts[0], points[i - 1][-1]):
                flatten += pts[1:]
            else:
                flatten += pts[::-1][1:]
        if is_same_point(flatten[0], flatten[-1]):
            flatten.pop(len(flatten) - 1)
    return flatten


def is_same_point(p1, p2):
    return (p1 == p2 or (p1 - p2).Length < 10**(-10))


def Moment_2(sketch, number_of_elements=200, min_num_per_wire=10):
    wires = sketch.Shape.Wires

    # check boundingbox diagonals to get outer shape
    diagonals = [wire.BoundBox.DiagonalLength for wire in wires]
    pos = diagonals.index(max(diagonals))

    # reaordering
    outer_wire = wires[pos]
    wires.pop(pos)
    wires.insert(0, outer_wire)

    Ix = 0
    Iy = 0
    panels = []
    panelvectors = []
    length_tot = sum(wire.Length for wire in wires)
    element_length = length_tot / number_of_elements

    for j, wire in enumerate(wires):
        edges = []
        for edge in wire.Edges:
            num = int(edge.Length // element_length)
            num += (number_of_elements < min_num_per_wire) * min_num_per_wire
            edges.append(edge.discretize(num))
        pts = check_connection(edges)
        pts = check_rotation(pts, outer=(j == 0))
        ix = 0
        iy = 0
        panelvectors_connected = []
        for pt in pts:
            point = PanelVector2(pt.x, pt.y)
            panelvectors_connected.append(point)
            panelvectors.append(point)
        panelvectors_connected.append(panelvectors_connected[0])
        for i, pt in enumerate(panelvectors_connected[:-1]):
            panels.append(DirichletPanel([pt, panelvectors_connected[i + 1]]))

            # one point integration
            diff = panels[-1].points[0] - panels[-1].points[1]
            ix += panels[-1].center.y ** 3 * diff.x / 3.
            iy += panels[-1].center.x ** 3 * diff.y / 3.

        Ix += ((j == 0)*2 - 1) * abs(ix)
        Iy += ((j == 0)*2 - 1) * abs(iy)
    case = TorsionCase(panels)
    case.run()
    print(Ix, Iy, Ix + Iy)
    print(case.torsion_moment)
    print(case.torsion_moment + Ix + Iy)

    nx = 100
    ny = 100
    bb = sketch.Shape.BoundBox
    x_grid = np.linspace(bb.XMin + 0.0001, bb.XMax - 0.0001, nx)
    y_grid = np.linspace(bb.YMin + 0.0001, bb.YMax - 0.0001, ny)
    grid = [Vector2(x, y) for y in y_grid for x in x_grid]
    t_list = [case.potential(point) for point in grid]
    stress_list = [case.stress(point) for point in grid]
    q_list = [case.q(point) for point in grid]
    writer = VtkWriter()

    with open(check_path("/tmp/heat_test.vtk"), "w") as _file:
        writer.structed_grid(_file, "element_2", [nx, ny, 1])
        writer.points(_file, grid)
        writer.data(_file, t_list, name="temperature", _type="SCALARS", data_type="POINT_DATA")
        writer.data(_file, stress_list, name="stress", _type="VECTORS", data_type="POINT_DATA")
        writer.data(_file, q_list, name="q", _type="VECTORS", data_type="POINT_DATA")


a = App.ActiveDocument.Sketch
print(Moment_2(a))