import os
import FreeCADGui as Gui
from PySide import QtGui
from pie_menu_3 import PieDialog
from beam import make_beam
from cut_miter import make_miter_cut
from cut_plane import make_plane_cut
from cut_shape import make_shape_cut
from set_simple import set_simple


view = Gui.ActiveDocument.ActiveView

def beam(): make_beam(view)
def miter(): make_miter_cut(view)
def plane(): make_plane_cut(view)
def shape(): make_shape_cut(view)
_dir = os.path.dirname(os.path.realpath(__file__))
command_list = [
    ["beam", beam, _dir + "/" + "icons/beam.svg"],
    ["miter", miter, _dir + "/" + "icons/beam_miter_cut.svg"],
    ["plane", plane, _dir + "/" + "icons/beam_plane_cut.svg"],
    ["shape", shape, _dir + "/" + "icons/beam_shape_cut.svg"],
    ["set_simple", set_simple, _dir + "/" + "icons/beam.svg"]]

beam_menue = PieDialog(QtGui.QKeySequence("b"), command_list)
action = QtGui.QAction(None)
action.setShortcut(beam_menue.key)
action.triggered.connect(beam_menue.showAtMouse)
Gui.getMainWindow().addAction(action)
