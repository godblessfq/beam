from PySide import QtGui
import FreeCADGui as Gui
import FreeCAD as App

def registerMenu(app):
    main_window=Gui.getMainWindow()
    menu_bar=main_window.menuBar()
    if menu_bar.findChild(QtGui.QMenu, "&MyMenu"):
        return
    app.menu_bar = menu_bar
    app.my_menu = menu_bar.addMenu("MyMenu")
    app.my_menu.setObjectName("&MyMenu")
    action = QtGui.QAction('test2', main_window)
    app.my_menu.addAction(action)

def registerToolBar(app):
    main_window = Gui.getMainWindow()
    app.my_toolbar = main_window.addToolBar("PartWorkbench: MyToolBar")
    dummy_action = QtGui.QAction(QtGui.QIcon(), "", app.my_toolbar)
    app.my_toolbar.addAction(dummy_action)
    app.my_toolbar.show()
registerToolBar(App)