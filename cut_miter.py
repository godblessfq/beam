from __future__ import division
import numpy as np

import FreeCAD as App
import FreeCADGui as Gui
import Part

from obj import CBeam, ViewProviderMiterCut


class make_miter_cut(object):
    def __init__(self, view):
        self.view = view
        App.Console.PrintMessage("choose a beam\n")
        self.klick_event = self.view.addEventCallback("SoMouseButtonEvent", self.choose_beam_1)

    def set_miter_cut_obj(self, obj, beam):
        if not obj.cut_type:
            obj.cut_obj = beam
            obj.cut_type = "miter"
            return
        return False

    def choose_beam_1(self, cb):
        if cb["State"] == "DOWN":
            sel = Gui.Selection.getSelection()
            if len(sel) > 0:
                self.beam_1 = sel[0]
                Gui.Selection.clearSelection()
                self.view.removeEventCallback("SoMouseButtonEvent", self.klick_event)
                self.klick_event = self.view.addEventCallback("SoMouseButtonEvent", self.choose_beam_2)
                App.Console.PrintMessage("choose another beam\n")

    def choose_beam_2(self, cb):
        if cb["State"] == "DOWN":
            sel = Gui.Selection.getSelection()
            if len(sel) > 0:
                self.beam_2 = sel[0]
                a = App.ActiveDocument.addObject("Part::FeaturePython", "miter_beam")
                CBeam(a, self.beam_1)
                self.set_miter_cut_obj(a, self.beam_2)
                ViewProviderMiterCut(a.ViewObject)

                a = App.ActiveDocument.addObject("Part::FeaturePython", "miter_beam")
                CBeam(a, self.beam_2)
                self.set_miter_cut_obj(a, self.beam_1)
                ViewProviderMiterCut(a.ViewObject)

                Gui.Selection.clearSelection()
                self.view.removeEventCallback("SoMouseButtonEvent", self.klick_event)
                App.ActiveDocument.recompute()

def to_np(app_vec):
    return np.array([app_vec.x, app_vec.y, app_vec.z])

if __name__ == "__main__":
    view = Gui.ActiveDocument.activeView()
    selection = make_miter_cut(view)
